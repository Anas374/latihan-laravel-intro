<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1><br>
    <h2>Sign Up</h2>
    <form action="/welcome" method="POST">
        @csrf
        <div>
            <label for="fn">First Name:</label><br>
            <input type="text" name="fn" id="fn"><br><br>
            <label for="ln">Last Name:</label><br>
            <input type="text" name="ln" id="ln">
        </div><br>
        <div>
            <label for="gender">Gender:</label><br>
            <input type="radio" name="gender" id="male" value="male">Male<br>
            <input type="radio" name="gender" id="female" value="female">Female<br>
            <input type="radio" name="gender" id="other" value="other">Other
        </div><br>
        <div>
            <label for="nationality">Nationality:</label><br>
            <select name="nationality" id="nationality">
                <option value="indonesian">Indonesian</option>
                <option value="american">American</option>
                <option value="wakanda">Wakanda</option>
                <option value="welsh">Welsh</option>
            </select>
        </div><br>
        <div>
            <label for="language">Language Spoken:</label><br>
            <input type="checkbox" name="language" id="bahasa" value="bahasa">Bahasa Indonesia<br>
            <input type="checkbox" name="language" id="english" value="english">English<br>
            <input type="checkbox" name="language" id="other" value="other">Other<br>
        </div><br>
        <div>
            <label for="bio">Bio:</label><br>
            <textarea name="bio" id="" cols="30" rows="10"></textarea>
        </div><br>
        <input type="submit" value="Submit">
        <input type="reset" value="Reset">
    </form>
</body>
</html>